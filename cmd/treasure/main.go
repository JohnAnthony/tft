package main

import (
	"flag"
	"fmt"

	"gitlab.com/JohnAnthony/godice"
)

type ring struct {
	name  string
	value uint
}

var (
	config struct {
		debug   bool
		verbose bool
	}

	state struct {
		rollCopper uint
		rollSilver uint
		rollGold   uint
		rollRing   uint

		coinsCopper uint
		coinsSilver uint
		coinsGold   uint
		rings       []ring
	}

	// Immutable
	ringCopper       = ring{"fine copper", 40}
	ringSilver       = ring{"silver", 75}
	ringSilverWJewel = ring{"silver with jewel", 120}
	ringGold         = ring{"gold", 200}
	ringGoldWJewel   = ring{"gold with jewel", 300}
)

func v(table string, roll uint, msg string) {
	if !config.verbose {
		return
	}
	fmt.Printf("INFO  [%7s,%2d]:\t%s\n", table, roll, msg)
}

func haveRolls() bool {
	return state.rollCopper > 0 || state.rollSilver > 0 || state.rollGold > 0 || state.rollRing > 0
}

func main() {
	flag.BoolVar(&config.debug, "d", false, "output debug info")
	flag.BoolVar(&config.verbose, "v", false, "output information about each step")
	flag.Parse()

	// Initial table roll
	{
		roll := uint(godice.Dice{Quantity: 1, Size: 6}.Eval())
		switch roll {
		case 1:
			v("initial", roll, "1x Copper Table")
			state.rollCopper += 1
		case 2:
			v("initial", roll, "2x Copper Table")
			state.rollCopper += 2
		case 3:
			v("initial", roll, "3x Copper Table")
			state.rollCopper += 3
		case 4:
			v("initial", roll, "1x Silver Table")
			state.rollSilver += 1
		case 5:
			v("initial", roll, "1x Silver Table, 1x Copper Table")
			state.rollCopper += 1
			state.rollSilver += 1
		case 6:
			v("initial", roll, "2x Copper Table")
			state.rollSilver += 2
		}
	}

	for {
		if config.debug {
			fmt.Printf(
				"STATE rolls[%d,%d,%d,%d] coins[%2d,%2d,%2d]",
				state.rollCopper,
				state.rollSilver,
				state.rollGold,
				state.rollRing,
				state.coinsCopper,
				state.coinsSilver,
				state.coinsGold,
			)
			for _, ring := range state.rings {
				fmt.Printf(" %s", ring.name)
			}
			fmt.Println("")
		}

		// Copper table
		if state.rollCopper > 0 {
			roll := uint(godice.Dice{Quantity: 2, Size: 6}.Eval())
			switch roll {
			case 2:
				v("copper", roll, "1x Silver Table")
				state.rollSilver += 1
			case 3:
				v("copper", roll, "2x Copper Table")
				state.rollCopper += 2
			case 4:
				add := uint(godice.Dice{Quantity: 4, Size: 6}.Eval())
				v("copper", roll, fmt.Sprintf("4d copper (%d)", add))
				state.coinsCopper += add
			case 5:
				add := uint(godice.Dice{Quantity: 3, Size: 6}.Eval())
				v("copper", roll, fmt.Sprintf("3d copper (%d)", add))
				state.coinsCopper += add
			case 6:
				fallthrough
			case 7:
				fallthrough
			case 8:
				add := uint(godice.Dice{Quantity: 1, Size: 6}.Eval()) + 1
				v("copper", roll, fmt.Sprintf("1d+1 copper (%d)", add))
				state.coinsCopper += add
			case 9:
				add := uint(godice.Dice{Quantity: 2, Size: 6}.Eval())
				v("copper", roll, fmt.Sprintf("2d copper (%d)", add))
				state.coinsCopper += add
			case 10:
				add := uint(godice.Dice{Quantity: 5, Size: 6}.Eval())
				v("copper", roll, fmt.Sprintf("5d copper (%d)", add))
				state.coinsCopper += add
			case 11:
				v("copper", roll, "3x Copper Table")
				state.rollCopper += 3
			case 12:
				v("copper", roll, "1x Silver Table")
				state.rollSilver += 1
			}

			state.rollCopper--
			continue
		}

		// Silver table
		if state.rollSilver > 0 {
			roll := uint(godice.Dice{Quantity: 2, Size: 6}.Eval())
			switch roll {
			case 2:
				v("silver", roll, "4x Silver Table")
				state.rollSilver += 4
			case 3:
				v("silver", roll, "2x Silver Table")
				state.rollSilver += 2
			case 4:
				add := uint(godice.Dice{Quantity: 4, Size: 6}.Eval())
				v("silver", roll, fmt.Sprintf("4d silver (%d)", add))
				state.coinsSilver += add
			case 5:
				add := uint(godice.Dice{Quantity: 3, Size: 6}.Eval())
				v("silver", roll, fmt.Sprintf("3d silver (%d)", add))
				state.coinsSilver += add
			case 6:
				fallthrough
			case 7:
				fallthrough
			case 8:
				add := uint(godice.Dice{Quantity: 1, Size: 6}.Eval()) + 1
				v("silver", roll, fmt.Sprintf("1d+1 silver (%d)", add))
				state.coinsSilver += add
			case 9:
				add := uint(godice.Dice{Quantity: 2, Size: 6}.Eval())
				v("silver", roll, fmt.Sprintf("2d silver (%d)", add))
				state.coinsSilver += add
			case 10:
				add := uint(godice.Dice{Quantity: 5, Size: 6}.Eval())
				v("silver", roll, fmt.Sprintf("5d silver (%d)", add))
				state.coinsSilver += add
			case 11:
				v("silver", roll, "3x Silver Table")
				state.rollSilver += 3
			case 12:
				v("silver", roll, "1x Gold Table")
				state.rollGold += 1
			}

			state.rollSilver--
			continue
		}

		// Gold table
		if state.rollGold > 0 {
			roll := uint(godice.Dice{Quantity: 1, Size: 6}.Eval())
			switch roll {
			case 1:
				v("gold", roll, "1 gold (%d)")
				state.coinsGold += 1
			case 2:
				v("gold", roll, "2 gold (%d)")
				state.coinsGold += 2
			case 3:
				add := uint(godice.Dice{Quantity: 1, Size: 6}.Eval())
				v("gold", roll, fmt.Sprintf("1d gold (%d)", add))
				state.coinsGold += add
			case 4:
				add := uint(godice.Dice{Quantity: 1, Size: 6}.Eval())
				v("gold", roll, fmt.Sprintf("1d gold (%d), 1x Silver Table", add))
				state.coinsGold += add
				state.rollSilver += 1
			case 5:
				v("gold", roll, "2x Gold Table")
				state.rollGold += 2
			case 6:
				v("gold", roll, "1x Ring Table")
				state.rollRing += 1
			}

			state.rollGold--
			continue
		}

		// Ring table
		if state.rollRing > 0 {
			roll := uint(godice.Dice{Quantity: 1, Size: 6}.Eval())
			switch roll {
			case 1:
				v("ring", roll, ringCopper.name)
				state.rings = append(state.rings, ringCopper)
			case 2:
				v("ring", roll, ringSilver.name)
				state.rings = append(state.rings, ringSilver)
			case 3:
				v("ring", roll, ringSilverWJewel.name)
				state.rings = append(state.rings, ringSilverWJewel)
			case 4:
				v("ring", roll, ringGold.name)
				state.rings = append(state.rings, ringGold)
			case 5:
				v("ring", roll, ringGoldWJewel.name)
				state.rings = append(state.rings, ringGoldWJewel)
			case 6:
				v("ring", roll, "1x Ring Table")
				state.rollRing += 2
			}

			state.rollRing--
			continue
		}

		// We didn't make any rolls - we're all done!
		break
	}

	// Print
	fmt.Printf("%dc %ds %dg\n", state.coinsCopper, state.coinsSilver, state.coinsGold)
	for _, ring := range state.rings {
		fmt.Printf("Ring! %s @ $%d\n", ring.name, ring.value)
	}
}
